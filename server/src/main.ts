import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication } from '@nestjs/common';

// tslint:disable-next-line: only-arrow-functions
async function bootstrap(): Promise<void> {
  const app: INestApplication = await NestFactory.create(AppModule);
  const port = 3000;
  await app.listen(port);
}
// tslint:disable-next-line: no-floating-promises
bootstrap();
