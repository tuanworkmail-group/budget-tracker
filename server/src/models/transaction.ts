import { TransactionType } from './transaction-type';
import { TransactionCategory } from './transaction-category';

export class Transaction {
    amount: number;
    time: Date;
    currentBalance: number | undefined;
    note: string;
    type: TransactionType;
    category: TransactionCategory | undefined;

    constructor(amount: number, time: Date, note: string, type: TransactionType) {
        this.amount = amount;
        this.time = time;
        this.note = note;
        this.type = type;
    }
}
